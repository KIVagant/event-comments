<?php
namespace Kivagant\Comment\Middleware;

use Kivagant\Comment\Filter\CommentFilter;
use Kivagant\Comment\Service\CommentServiceInterface;
use Kivagant\Comment\Service\CommentService;
use Kivagant\Exception\BadRequestException;
use Kivagant\Middleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class CommentCreateMiddleware implements MiddlewareInterface
{
    /**
     * @var CommentServiceInterface|CommentService
     */
    protected $service;

    public function __construct(CommentServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param ServerRequestInterface|\Kivagant\Http\ServerRequest $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        // Load comment from the query string or from the POST body
        $comment = $request->getQueryParam('comment', $request->getParsedBodyParam('comment'));
        if (!$comment) {
            throw new BadRequestException('Comment text cannot be empty');
        }
        $filter = (new CommentFilter())
            ->setComment($comment);
        $data = $this->service->add($filter);

        /** @var \Kivagant\Http\ArrayResponse $response */
        $response->setContent($data->toArray());

        return $next($request, $response);
    }
}