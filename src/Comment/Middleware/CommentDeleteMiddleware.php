<?php
namespace Kivagant\Comment\Middleware;

use Kivagant\Comment\Filter\CommentFilter;
use Kivagant\Comment\Service\CommentServiceInterface;
use Kivagant\Comment\Service\CommentService;
use Kivagant\Exception\BadRequestException;
use Kivagant\Middleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class CommentDeleteMiddleware implements MiddlewareInterface
{
    /**
     * @var CommentServiceInterface|CommentService
     */
    protected $service;

    public function __construct(CommentServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param ServerRequestInterface|\Kivagant\Http\ServerRequest $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $id = $request->getAttribute('id');
        if (!$id) {
            throw new BadRequestException('Id cannot be empty');
        }
        $filter = (new CommentFilter())->setId($id);
        $this->service->delete($filter);

        /** @var \Kivagant\Http\ArrayResponse $response */
        $response->setContent(['deleted' => $filter->getId()]);

        return $next($request, $response);
    }
}