<?php
namespace Kivagant\Comment\Middleware;

use Kivagant\Comment\Filter\CommentFilter;
use Kivagant\Comment\Service\CommentServiceInterface;
use Kivagant\Comment\Service\CommentService;
use Kivagant\Exception\BadRequestException;
use Kivagant\Middleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class CommentReadMiddleware implements MiddlewareInterface
{
    /**
     * @var CommentServiceInterface|CommentService
     */
    protected $service;

    public function __construct(CommentServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param ServerRequestInterface|\Kivagant\Http\ServerRequest $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $id = $request->getAttribute('id');
        $filter = new CommentFilter();
        if ($id) {
            $filter = $filter->setId($id);
        }
        $data = $this->service->read($filter);

        /** @var \Kivagant\Http\ArrayResponse $response */
        $response->setContent($this->transform($data->toArray()));

        return $next($request, $response);
    }

    protected function transform(array $data = [])
    {
        foreach ($data as &$item) {
            $item['comment'] = str_replace(':)', '{smile}', $item['comment']);
        }

        return $data;
    }
}