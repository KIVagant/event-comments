<?php
namespace Kivagant\Comment\Event;

use Kivagant\Comment\Entity\CommentEntity;
use Kivagant\EventManager\EventInterface;

class CommentAddedEvent implements EventInterface
{
    protected $entity;

    public function __construct(CommentEntity $entity)
    {
        $this->entity = $entity;
    }

    public function getEntity() : CommentEntity
    {
        return $this->entity;
    }

}