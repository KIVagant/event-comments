<?php
namespace Kivagant\Comment\Listener;

use Kivagant\Comment\Event\CommentAddedEvent;
use Kivagant\EventManager\EventInterface;
use Kivagant\EventManager\EventListenerInterface;

class CommentParserListener implements EventListenerInterface
{
    public function __invoke(EventInterface $event)
    {
        $this->proceed($event);
    }

    protected function proceed(CommentAddedEvent $event)
    {
        // Of course, it is not a good idea to catch event and save html to the database.
        // So, this listener will catch event AFTER insert and, for example, could send an notification emails.
        // Normal view transformation executed in \Kivagant\Comment\Middleware\CommentReadMiddleware::transform
        $comment = $event->getEntity();
        $comment->setComment(str_replace(':)', '{smile}', $comment->getComment()));
    }
}