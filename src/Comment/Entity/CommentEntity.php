<?php
namespace Kivagant\Comment\Entity;

use Kivagant\Entity\EntityInterface;

class CommentEntity implements EntityInterface
{
    protected $id = 0;
    protected $comment = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id = 0): self
    {
        $this->id = (int)$id;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return CommentEntity
     */
    public function setComment($comment)
    {
        $this->comment = (string)$comment;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'comment' => $this->comment,
        ];
    }
}