<?php
namespace Kivagant\Comment\Service;

use Kivagant\Comment\Entity\CommentEntity;
use Kivagant\Comment\Event\CommentAddedEvent;
use Kivagant\Comment\Filter\CommentFilter;
use Kivagant\Entity\EntityCollection;
use Kivagant\EventManager\EventManagerAwareTrait;
use Kivagant\EventManager\EventManagerInterface;
use Kivagant\Comment\Model\CommentModel;

class CommentService implements CommentServiceInterface
{
    use EventManagerAwareTrait;

    /**
     * @var CommentModel
     */
    protected $model;

    public function __construct(EventManagerInterface $dispatcher, CommentModel $model)
    {
        $this->dispatcher = $dispatcher;
        $this->model = $model;
    }

    public function add($filter) : CommentEntity
    {
        $entity = $this->model->add($filter);

        $this->fireCommentAdded($entity);

        return $entity;
    }

    public function read(CommentFilter $filter) : EntityCollection
    {
        return $this->model->read($filter);
    }

    public function delete(CommentFilter $filter)
    {
        return $this->model->delete($filter);
    }

    /**
     * @param CommentEntity|string $entity
     */
    protected function fireCommentAdded(CommentEntity $entity)
    {
        $event = new CommentAddedEvent($entity);
        $this->getDispatcher()->dispatch($event);
    }
}