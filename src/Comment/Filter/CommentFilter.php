<?php
namespace Kivagant\Comment\Filter;

/**
 * @immutable
 */
class CommentFilter
{
    protected $id = 0;
    protected $comment = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId($id = 0): self
    {
        $filter = clone $this;
        $filter->id = (int)$id;

        return $filter;
    }

    public function getComment(): string
    {
        return $this->comment;
    }

    public function setComment($comment): self
    {
        $filter = clone $this;
        $filter->comment = (string)$comment;

        return $filter;
    }

    public function getUuid()
    {
        $string = __CLASS__;
        foreach ($this as $item) {
            if (is_string($item) || is_scalar($item)) {
                $string .= $item;
            }
        }

        return md5($string);
    }
}