<?php
namespace Kivagant\Comment\Model\Table;

use Kivagant\Comment\Entity\CommentEntity;
use Kivagant\Comment\Filter\CommentFilter;
use Kivagant\Entity\EntityCollection;
use Kivagant\Layers\Exception\DeleteErrorException;
use Kivagant\Layers\Exception\InsertErrorException;
use Kivagant\Layers\Sqlite\ConnectionInterface;
use Kivagant\Layers\Sqlite\Sqlite;

class CommentTable
{
    protected $table = 'comments';
    protected $primary_key = 'id';

    /**
     * @var ConnectionInterface|Sqlite
     */
    protected $connection;

    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return \SQLite3Result
     */
    public function dropTable()
    {
        $query = 'DROP TABLE IF EXISTS ' . $this->table;

        return $this->query($query);
    }

    /**
     * @return \SQLite3Result
     */
    public function createTable()
    {
        $query = 'CREATE TABLE IF NOT EXISTS '
            . $this->table
            . ' (' . $this->primary_key . ' INTEGER PRIMARY KEY, comment STRING)';

        return $this->query($query);
    }

    public function read(CommentFilter $filter) : EntityCollection
    {
        $collection = new EntityCollection();
        $query = 'SELECT * FROM ' . $this->table . ' ';
        if ($filter->getId()) {
            $query .= ' WHERE ' . $this->primary_key . '=' . $filter->getId();
        }
        $result = $this->query($query);

        if (!$result) {

            return $collection;
        }
        while ($row = $result->fetchArray()) {
            $entity = new CommentEntity();
            $entity->setId($row['id']);
            $entity->setComment($row['comment']);

            $collection->append($entity);
        }

        return $collection;
    }

    /**
     * @param CommentFilter $filter
     * @return CommentEntity
     * @throws InsertErrorException
     */
    public function add(CommentFilter $filter)
    {
        $comment = $this->connection->escape($filter->getComment());
        $query = 'INSERT INTO ' . $this->table . ' (comment) VALUES ("' . $comment . '") ';

        $result = $this->query($query);
        if (!$result) {
            $this->throwInsertError();
        }

        return (new CommentEntity())
            ->setId($this->getLastRowId())
            ->setComment($comment)
        ;
    }

    /**
     * @param CommentFilter $filter
     * @return bool
     * @throws DeleteErrorException
     */
    public function delete(CommentFilter $filter)
    {
        $query = 'DELETE FROM ' . $this->table . ' WHERE ' . $this->primary_key . '=' . $filter->getId();

        $result = $this->query($query);
        if (!$result) {
            $this->throwDeleteError();
        }

        return true;
    }
    /**
     * @param $query
     * @return \SQLite3Result
     */
    protected function query($query)
    {

        return $this->connection->getResource()->query($query);
    }

    /**
     * @return int
     */
    protected function getLastRowId()
    {

        return $this->connection->getResource()->lastInsertRowID();
    }

    protected function throwInsertError()
    {
        $resource = $this->connection->getResource();
        throw new InsertErrorException($resource->lastErrorMsg(), $resource->lastErrorCode());
    }

    protected function throwDeleteError()
    {
        $resource = $this->connection->getResource();
        throw new DeleteErrorException($resource->lastErrorMsg(), $resource->lastErrorCode());
    }
}