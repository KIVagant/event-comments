<?php
namespace Kivagant\Comment\Model;

use Kivagant\Comment\Filter\CommentFilter;
use Kivagant\Comment\Model\Table\CommentTable;
use Kivagant\Layers\Cache\CacheInterface;

class CommentModel
{
    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var CommentTable
     */
    protected $table;

    public function __construct(CacheInterface $cache, CommentTable $table)
    {
        $this->cache = $cache;
        $this->table = $table;
    }

    public function add(CommentFilter $filter)
    {
        return $this->table->add($filter);
    }
    public function read(CommentFilter $filter)
    {
// You can reset the database with this simple commands
//        $this->table->dropTable();
//        $this->table->createTable();

        $key = $filter->getUuid();
        $data = $this->cache->get($key);
        if ($data === false) {
            $data = $this->table->read($filter);
            $this->cache->add($key, $data);
        }

        return $data;
    }
    public function delete(CommentFilter $filter)
    {
        return $this->table->delete($filter);
    }
}