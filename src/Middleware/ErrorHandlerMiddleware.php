<?php
namespace Kivagant\Middleware;

use Kivagant\Config\ConfigInterface;
use Kivagant\Exception\BadRequestException;
use Kivagant\Http\StringResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class ErrorHandlerMiddleware implements MiddlewareInterface
{
    protected $response;

    protected $request;

    protected $debug;

    public function __construct(ConfigInterface $config)
    {
        $this->debug = (bool)$config->get('application.debug');
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $this->request = $request;
        $this->response = $response;
        try {
            return $next($this->request, $this->response);
        } catch (BadRequestException $e) {
            $this->badRequest($e);
        } catch (\Exception $e) {
            $this->error($e);
        } catch (\Throwable $e) {
            $this->error($e);
            if ($this->debug) {
                throw $e;
            }
        }
    }

    protected function badRequest($e)
    {
        $response = new StringResponse($e->getMessage());
        $response->withStatus(400, $e->getMessage());
        $response->send();
    }
    protected function error($e)
    {
        $response = new StringResponse('Internal Error');
        $response->withStatus(503, '503 Internal Error: ' . $e->getMessage());
        $response->send();
    }
}