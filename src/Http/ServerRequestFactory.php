<?php
namespace Kivagant\Http;

use Kivagant\Factory\FactoryInterface;
use Kivagant\Router\RouteInterface;

class ServerRequestFactory implements FactoryInterface
{
    /**
     * @var RouteInterface
     */
    protected $route;

    public function __construct(RouteInterface $route)
    {
        $this->route = $route;
    }

    public function __invoke()
    {
        $headers = $this->parseHeaders($_SERVER);
        $request = (new ServerRequest($this->route->getParams(), $headers))
            ->setRoute($this->route)
            ->withCookieParams($_COOKIE)
            ->withQueryParams($_GET)
            ->withParsedBody($_POST)
        ;

        return $request;
    }

    protected function parseHeaders(array $server)
    {
        $headers = [];
        foreach ($server as $key => $value) {
            if ($value && strpos($key, 'HTTP_') === 0) {
                $name = strtr(substr($key, 5), '_', ' ');
                $name = strtr(ucwords(strtolower($name)), ' ', '-');
                $name = strtolower($name);

                $headers[$name] = array_map('trim', explode(',', $value));
                continue;
            }

            if ($value && strpos($key, 'CONTENT_') === 0) {
                $name = substr($key, 8); // Content-
                $name = 'Content-' . (($name == 'MD5') ? $name : ucfirst(strtolower($name)));
                $name = strtolower($name);
                $headers[$name] = array_map('trim', explode(',', $value));
                continue;
            }
        }

        return $headers;
    }
}