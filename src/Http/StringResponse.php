<?php
namespace Kivagant\Http;

/**
 * Warning: This class is just a stub and is NOT immutable. Use another libraries for full interface implementation.
 */
class StringResponse extends ApplicationResponseAbstract implements StringResponseInterface
{
    /**
     * @var string
     */
    protected $content = '';

    public function __construct($content = '', $status = 200, array $headers = [])
    {
        parent::__construct(null, $status, $headers);
        $this->content = $content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content = '')
    {
        $this->content = (string)$content;

        return $this;
    }

    protected function prepare()
    {
        return $this->content;
    }
}