<?php
namespace Kivagant\Router;

use Kivagant\Exception\RuntimeException;

class Router implements RouterInterface
{
    const REGEXP_TOKENS = '/{(?P<token>[^}]*)}/';
    const REGEXP_TOKEN = '/(?P<opt>\?)?(?P<name>[a-z]*:)?(?P<pattern>.*)/i';
    const EMPTY_TOKEN_NAME = 'param_';

    /**
     * @var array [ 'name' => 'path' ]
     */
    protected $paths = [];

    /**
     * @var array of RouteInterface
     */
    protected $routes = [];

    public function add(RouteInterface $route) : RouterInterface
    {
        $name = (string)$route->getName();
        if (!$name) {
            throw new RuntimeException('Route name can not be empty');
        }
        if (!$route->getPath()) {
            throw new RuntimeException('Route path can not be empty');
        }
        if (array_key_exists($name, $this->routes)) {
            throw new RuntimeException('Route with the name ' . $name . ' is already registered');
        }
        $this->routes[$name] = $route;

        return $this;
    }

    public function match(string $url, string $method = RouterInterface::METHOD_GET) : RouteInterface
    {
        foreach ($this->routes as $routeName => $route) {
            /** @var RouteInterface $route */
            $routePath = $route->getPath();
            if ($url === $routePath && in_array($method, $route->getMethods(), true)) {

                return $route;
            }
            $matches = $this->matchTokenList($routePath);
            if (!empty($matches['token'])) {
                list($tokens, $getParamsRegexp) = $this->matchTokens($matches['token'], $routePath);
                list($result, $params) = $this->matchParams($url, $getParamsRegexp, $tokens);
                if ($result && in_array($method, $route->getMethods(), true)) {
                    return $route->setParams($params);
                }
            }
        }

        return new RouteEmpty();
    }

    protected function normalizeParams(array $params = []) : array
    {
        foreach ($params as $name => $param) {
            $params[$name] = str_replace('/', '', $param);
        }

        return $params;
    }

    protected function matchParams(string $url, string $getParamsRegexp, array $tokens) : array
    {
        $params = [];
        $matched = preg_match($getParamsRegexp, $url, $params);
        $params = array_filter($params, function ($name, $key) use ($tokens) {
            return array_key_exists($key, $tokens);
        }, ARRAY_FILTER_USE_BOTH);
        $params = $this->normalizeParams($params);

        return [$matched, $params];
    }

    protected function matchTokens($matchedTokens, $getParamsRegexp)
    {
        $tokens = [];
        $tokenCount = 0;
        $tokensMatch = [];
        foreach ($matchedTokens as $token) {
            preg_match(self::REGEXP_TOKEN, $token, $tokensMatch);
            $tokenName = str_replace(':', '', $tokensMatch['name']);
            $tokenName = $tokenName ?: self::EMPTY_TOKEN_NAME . $tokenCount;
            $tokens[$tokenName]['name'] = $tokenName;
            $tokens[$tokenName]['token'] = '{' . $token . '}';
            $tokens[$tokenName]['pattern'] = $tokensMatch['pattern'];
            $tokens[$tokenName]['regexp'] = '(?P<' . $tokenName . '>' . $tokensMatch['pattern'] . ')' . $tokensMatch['opt'];
            $tokens[$tokenName]['optional'] = $tokensMatch['opt'];
            $tokenCount++;
            $getParamsRegexp = str_replace($tokens[$tokenName]['token'], $tokens[$tokenName]['regexp'],
                $getParamsRegexp);
        }
        $getParamsRegexp = '/' . str_replace('/', '\/', $getParamsRegexp) . '/i';

        return array($tokens, $getParamsRegexp);
    }

    protected function matchTokenList(string $routePath) : array
    {
        $matches = [];
        preg_match_all(self::REGEXP_TOKENS, $routePath, $matches);

        return $matches;
    }
}