<?php
namespace Kivagant\Router;

class RouteEmpty implements RouteInterface
{
    public function getName(): string
    {
        return '';
    }

    public function getPath(): string
    {
        return '';
    }

    public function getParams(): array
    {
        return [];
    }

    public function getOptions(): array
    {
        return [];
    }

    public function hasParam($name): bool
    {
        return false;
    }

    public function getParam($name, $default = null)
    {
        return $default;
    }

    public function hasOption($name): bool
    {
        return false;
    }

    public function getOption($name, $default = null)
    {
        return $default;
    }

    public function getMethods(): array
    {
        return [];
    }

    public function hasMethod($name): bool
    {
        return false;
    }

    /**
     * @immutable
     * @param $params
     * @return RouteEmpty|RouteInterface
     */
    public function setParams($params): RouteInterface
    {
        return $this;
    }
}