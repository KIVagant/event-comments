<?php
namespace Kivagant\Router;

class Route implements RouteInterface
{
    protected $name = '';
    protected $path = '';
    protected $params = [];
    protected $options = [];
    protected $methods = [];

    public function __construct($name, $path, array $methods = [], array $params = [], array $options = [])
    {
        $this->name = $name;
        $this->path = $path;
        $this->methods = $methods;
        $this->params = $params;
        $this->options = $options;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getParams(): array
    {
        return $this->params;
    }
    public function hasParam($name): bool
    {
        return array_key_exists($name, $this->params);
    }
    public function getParam($name, $default = null)
    {
        return $this->hasParam($name) ? $this->params[$name] : $default;
    }

    public function getOptions(): array
    {
        return $this->options;
    }
    public function hasOption($name): bool
    {
        return array_key_exists($name, $this->options);
    }
    public function getOption($name, $default = null)
    {
        return $this->hasOption($name) ? $this->options[$name] : $default;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }
    public function hasMethod($name): bool
    {
        return in_array($name, $this->methods, true);
    }

    /**
     * @immutable
     * @param array $params
     * @return self|RouteInterface
     */
    public function setParams($params): RouteInterface
    {
        $route = clone $this;
        $route->params = $params;

        return $route;
    }
}