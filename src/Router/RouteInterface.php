<?php
namespace Kivagant\Router;

interface RouteInterface
{
    public function getName(): string;
    public function getPath(): string;

    public function getParams(): array;
    public function hasParam($name): bool;
    public function getParam($name, $default = null);

    public function getOptions(): array;
    public function hasOption($name): bool;
    public function getOption($name, $default = null);

    public function getMethods(): array;
    public function hasMethod($name): bool;

    /**
     * @immutable
     * @param $params
     * @return RouteInterface
     */
    public function setParams($params): self;
}