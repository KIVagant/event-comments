<?php
namespace Kivagant\Router;

interface RouterInterface
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';

    public function add(RouteInterface $route): self;
    public function match(string $url, string $method = self::METHOD_GET): RouteInterface;
}