<?php
namespace Kivagant\Entity;

class EntityCollection implements \Iterator, EntityCollectionInterface
{
    protected $entities = [];
    protected $position = 0;

    public function append(EntityInterface $entity)
    {
        array_push($this->entities, $entity);

        return $this;
    }

    public function prepend(EntityInterface $entity)
    {
        array_unshift($this->entities, $entity);
        return $this;
    }

    public function detach(EntityInterface $entity)
    {
        foreach ($this->entities as $key => $currentEntity) {
            if ($currentEntity == $entity) {
                unset($this->entities[$key]);

                return true;
            }
        }

        return false;
    }

    public function remove($position = null)
    {
        if (null === $position) {
            $position = $this->position;
        }

        unset($this->entities[$position]);
    }

    public function attachCollection(EntityCollection $collection)
    {
        foreach ($collection as $entity) {
            $this->append($entity);
        }
    }
    public function current()
    {
        return current($this->entities);
    }

    public function next()
    {
        $this->position++;
        return next($this->entities);
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        $key = key($this->entities);
        return ($key !== null);
    }

    public function rewind()
    {
        reset($this->entities);
        $this->position = 0;
    }

    public function toArray()
    {
        $return = [];
        foreach ($this->entities as $entity) {
            /** @var $entity EntityInterface */
            $return[] = $entity->toArray();
        }
        $this->rewind();

        return $return;
    }
    public function count()
    {
        return count($this->entities);
    }
}