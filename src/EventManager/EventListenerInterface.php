<?php
namespace Kivagant\EventManager;

interface EventListenerInterface
{
    public function __invoke(EventInterface $event);
}