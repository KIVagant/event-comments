<?php
namespace Kivagant\EventManager;

trait EventManagerAwareTrait
{
    /**
     * @var EventManagerInterface|EventManager
     */
    protected $dispatcher;

    protected function getDispatcher() : EventManagerInterface
    {
        return $this->dispatcher;
    }
}