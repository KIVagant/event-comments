<?php
namespace Kivagant\EventManager;

class EventManager implements EventManagerInterface
{
    protected $listeners = [];

    public function listen(string $event_name, callable $callback)
    {
        $this->listeners[$event_name][] = $callback;
    }

    public function dispatch(EventInterface $event)
    {
        $event_name = get_class($event);
        if (!array_key_exists($event_name, $this->listeners)) {

            return;
        }
        foreach ($this->listeners[$event_name] as $listener) {
            $listener($event);
        }
    }
}