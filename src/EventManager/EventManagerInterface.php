<?php
namespace Kivagant\EventManager;

interface EventManagerInterface
{
    public function listen(string $event_name, callable $callback);
    public function dispatch(EventInterface $event);
}