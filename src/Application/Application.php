<?php
namespace Kivagant\Application;

use Interop\Container\ContainerInterface;
use Kivagant\EventManager\EventInterface;
use Kivagant\EventManager\EventListenerInterface;
use Kivagant\Exception\ObjectNotFoundException;
use Kivagant\ServiceContainer\ServiceContainer;
use Kivagant\Config\Config;
use Kivagant\Config\ConfigInterface;
use Kivagant\EventManager\EventManagerAwareTrait;
use Kivagant\EventManager\EventManagerInterface;
use Kivagant\Http\ApplicationResponseInterface;
use Kivagant\Http\ArrayResponse;
use Kivagant\Http\ServerRequest;
use Kivagant\Http\StringResponse;
use Kivagant\Pipeline\PipelineMiddleware;
use Kivagant\Router\Route;
use Kivagant\Router\RouteInterface;
use Kivagant\Router\RouteEmpty;
use Kivagant\Router\Router;
use Kivagant\Router\RouterInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class Application implements ApplicationInterface
{
    use EventManagerAwareTrait;
    /**
     * @var ConfigInterface|Config
     */
    protected $config;

    /**
     * @var RouterInterface|Router
     */
    protected $router;

    /**
     * @var RouteInterface|Route
     */
    protected $route;

    /**
     * @var ServerRequestInterface|ServerRequest
     */
    protected $request;

    /**
     * @var ResponseInterface|ArrayResponse
     */
    protected $response;

    /**
     * @var ContainerInterface|ServiceContainer
     */
    protected $container;

    public function __construct(
        ConfigInterface $config,
        RouterInterface $router,
        EventManagerInterface $dispatcher,
        ContainerInterface $container
    )
    {
        $this->config = $config;
        $this->router = $router;
        $this->dispatcher = $dispatcher;
        $this->container = $container;
    }

    public function run()
    {
        $this->attachListeners();
        $this->fillRoutes();
        $this->machRoute(array_key_exists('PATH_INFO', $_SERVER) ? $_SERVER['PATH_INFO'] : '/', $_SERVER['REQUEST_METHOD']);
        $this->requestFactory();
        $this->responseFactory();
        $this->runPipeline();
        $this->sendResponse();
    }

    protected function attachListeners()
    {
        $events_map = $this->config->get('events.subscribers');
        foreach ($events_map as $event => $subscribers) {
            if (!class_exists($event)) {
                throw new ObjectNotFoundException('Event class not found: "' . $event . '"');
            }
            if (is_array($subscribers)) {
                foreach ($subscribers as $subscriber) {
                    $this->subscribe($event, $subscriber);
                }
            } elseif (is_string($subscribers)) {
                $this->subscribe($event, $subscribers);
            }
        }
    }

    protected function fillRoutes()
    {
        $routes = $this->config->get('routes', []);
        foreach ($routes as $route) {
            $route = new Route($route['name'], $route['path'], $route['methods'], [], $route['options']);
            $this->router->add($route);
        }
    }

    protected function machRoute($url, $method)
    {
        $this->route = $this->router->match($url, $method);
        $this->container->add(RouteInterface::class, $this->route);
    }

    protected function requestFactory()
    {
        $this->request = $this->container->get(ServerRequestInterface::class);
    }

    protected function responseFactory()
    {
        if ($this->route instanceof RouteEmpty) {
            $this->setNotFoundResponse();
        } else {
            $this->response = $this->container->get(ApplicationResponseInterface::class);
        }
    }

    protected function runPipeline()
    {
        $middlewareName = null;
        $pipe = new PipelineMiddleware($this->request, $this->response);
        foreach ($this->route->getOption('middlewares', []) as $middlewareName) {
            $pipe = $pipe->pipe($this->container->get($middlewareName));
        }
        if ($middlewareName) {
            $this->response = $pipe();

            return;
        }

        $this->setNotFoundResponse();
    }

    protected function sendResponse()
    {
        if ($this->response instanceof ApplicationResponseInterface) {
            $this->response->send();
        }
    }

    /**
     * @param $event
     * @param $subscriber
     */
    protected function subscribe($event, $subscriber)
    {
        if (!class_exists($subscriber)) {
            throw new ObjectNotFoundException('Event listener class not found: "' . $subscriber . '"');
        }
        $container = $this->container;
        $this->dispatcher->listen(
            $event,
            function (EventInterface $event) use ($container, $subscriber) {

                /** @var EventListenerInterface $listener */
                $listener = $container->get($subscriber);

                return $listener($event);
            }
        );
    }

    protected function setNotFoundResponse()
    {
        $this->response = new StringResponse('Page Not Found');
        $this->response->withStatus(404, 'Not Found');
    }
}