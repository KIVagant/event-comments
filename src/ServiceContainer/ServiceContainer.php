<?php
namespace Kivagant\ServiceContainer;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Interop\Container\Exception\NotFoundException;
use Kivagant\Exception\ContainerErrorException;
use Kivagant\Exception\ObjectNotFoundException;
use Kivagant\Factory\FactoryInterface;

class ServiceContainer implements ContainerInterface
{
    /**
     * All services
     *
     * @var array
     */
    protected $services;

    /**
     * The services which have an instance
     *
     * @var array
     */
    protected $instantiated;

    /**
     * True if a service can be shared
     *
     * @var array
     */
    protected $shared;

    public function __construct()
    {
        $this->services = array();
        $this->instantiated = array();
        $this->shared = array();
    }

    /**
     * Registers a service with specific interface.
     *
     * @param string $interface
     * @param string|object $service
     * @param bool $share
     */
    public function add($interface, $service, $share = true)
    {
        if (is_object($service) && $share) {
            $this->instantiated[$interface] = $service;
        }
        $this->services[$interface] = (is_object($service) ? get_class($service) : $service);
        $this->shared[$interface] = $share;
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundException  No entry was found for this identifier.
     * @throws ContainerException Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id)
    {
        if (array_key_exists($id, $this->instantiated) && $this->shared[$id]) {

            return $this->instantiated[$id];
        }

        if (!array_key_exists($id, $this->services)) {
            throw new ObjectNotFoundException(
                'Cannot instantiate the object "'
                . $id . '" that is not registered in the container'
            );
        }

        $service = $this->services[$id];
        if (!class_exists($service)) {
            throw new ContainerErrorException('Class "' . $service . '" is not exist');
        }

        if (!$object = $this->constructorDI($service)) {
            $object = new $service();
        }

        if ($object instanceof FactoryInterface) {
            $object = $object();
        }

        if ($this->shared[$id]) {
            $this->instantiated[$id] = $object;
        }

        return $object;
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return boolean
     */
    public function has($id)
    {
        return array_key_exists($id, $this->services) || array_key_exists($id, $this->instantiated);
    }

    /**
     * The simple DI implementation
     *
     * @param $service
     * @return object|null
     */
    protected function constructorDI($service)
    {
        $reflection = new \ReflectionClass($service);
        $constructor = $reflection->getConstructor();
        if ($constructor instanceof \ReflectionMethod) {
            $constructorArgs = $constructor->getParameters();
            $instances = [];
            $count = 1;
            /** @var \ReflectionParameter $argument */
            foreach ($constructorArgs as $argument) {
                $class = $argument->getClass();
                if ($class instanceof \ReflectionClass) {
                    $class_name = $class->getName();
                    if (!$this->has($class_name)) {
                        throw new ObjectNotFoundException(
                            'Cannot instantiate the object "' . $service
                            . '". Argument #' . $count . ' with the name "'
                            . $argument->getName() . '" is not registered in the container.');
                    }
                    $instances[$argument->getName()] = $this->get($class_name);
                } else {
                    if (!$argument->isDefaultValueAvailable()) {
                        throw new ContainerErrorException(
                            'Cannot instantiate the object "' . $service
                            . '". Argument #' . $count . ' with the name "'
                            . $argument->getName() . '" is not an object and does not have a default value.');
                    }
                    $instances[$argument->getName()] = $argument->getDefaultValue();
                }
                $count++;
            }
            $object = $reflection->newInstanceArgs($instances);

            return $object;
        }

        return false;
    }
}