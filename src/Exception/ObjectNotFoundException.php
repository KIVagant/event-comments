<?php
namespace Kivagant\Exception;

use Interop\Container\Exception\NotFoundException as NotFoundExceptionInterface;

class ObjectNotFoundException extends \RuntimeException implements NotFoundExceptionInterface
{

}