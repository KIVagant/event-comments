<?php
namespace Kivagant\Exception;

class NotImplementedException extends \RuntimeException
{
    public function __construct($code = 0, \Exception $previous = null)
    {
        parent::__construct('Sorry, the method is not implemented', $code, $previous);
    }

}