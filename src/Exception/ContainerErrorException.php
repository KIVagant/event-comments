<?php
namespace Kivagant\Exception;

use Interop\Container\Exception\ContainerException as ContainerExceptionInterface;

class ContainerErrorException extends \RuntimeException implements ContainerExceptionInterface
{

}