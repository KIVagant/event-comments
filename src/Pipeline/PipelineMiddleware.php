<?php
namespace Kivagant\Pipeline;

use Kivagant\Middleware\MiddlewareInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class PipelineMiddleware implements PipelineMiddlewareInterface
{
    /**
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var array of callable
     */
    protected $middlewares = [];

    public function __construct(ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function pipe(MiddlewareInterface $middleware)
    {
        $pipeline = clone $this; // Immutability
        $pipeline->middlewares[] = $middleware;

        return $pipeline;
    }

    public function process()
    {
        if (0 === count($this->middlewares)) {

            return $this->response;
        }
        return $this->next($this->middlewares);
    }

    public function __invoke()
    {

        return $this->process();
    }

    protected function next(array $middlewares)
    {
        return call_user_func(
            $this->getNextCallable($middlewares)
            , $this->request
            , $this->response
        );
    }

    protected function getNextCallable(array $middlewares)
    {
        $middleware = null;
        if (is_array($middlewares)) {

            $middleware = array_shift($middlewares);
        }

        return is_callable($middleware) ? $this->wrapCallable($middleware, $middlewares) : $this->getFinalCallable();
    }

    protected function getFinalCallable(): callable
    {
        return function (
            RequestInterface $request = null,
            ResponseInterface $response = null,
            callable $next = null
        ) {

            return $response;
        };
    }

    private function wrapCallable(callable $middleware, array $middlewares)
    {
        $service = $this;
        return function (
            RequestInterface $request = null,
            ResponseInterface $response = null,
            callable $next = null
        ) use ($service, $middleware, $middlewares) {

            $next = $service->getNextCallable($middlewares);

            return call_user_func($middleware, $request, $response, $next);
        };
    }
}