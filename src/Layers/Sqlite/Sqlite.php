<?php
namespace Kivagant\Layers\Sqlite;

use Kivagant\Config\ConfigInterface;
use Kivagant\Layers\Exception\ConnectionErrorException;

class Sqlite implements ConnectionInterface
{
    /**
     * @var \SQLite3
     */
    protected $resource;

    /**
     * @var string
     */
    protected $storagePath;

    public function __construct(ConfigInterface $config)
    {
        $this->storagePath = $config->get('storage.sqlite.path');
        if (!$this->storagePath) {
            throw new ConnectionErrorException('Empty Sqlite storage path');
        }
        $this->connect();
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    public function connect() : \SQLite3
    {
        if (!$this->isConnected()) {
            $this->resource = new \SQLite3($this->storagePath);
        }

        return $this->resource;
    }

    public function disconnect()
    {
        if ($this->isConnected()) {
            $this->resource->close();
        }
    }

    public function getResource() : \SQLite3
    {
        return $this->resource;
    }

    public function isConnected(): bool
    {
        return $this->resource instanceof \SQLite3;
    }

    public function escape($data)
    {
        if(is_array($data)) {

            return array_map([\SQLite3::class, 'escapeString'], $data);
        }

        return \SQLite3::escapeString($data);
    }
}