<?php
namespace Kivagant\Layers\Sqlite;

interface ConnectionInterface
{
    public function connect();
    public function disconnect();

    /**
     * @return resource|null
     */
    public function getResource();
    public function isConnected(): bool;
}