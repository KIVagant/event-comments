<?php
use Interop\Container\ContainerInterface;
use Kivagant\Config\Config;
use Kivagant\Config\ConfigInterface;
use Kivagant\ServiceContainer\ServiceContainer;

$container = new ServiceContainer();
$container->add(ContainerInterface::class, $container, true);

$config = (new Config())->loadDirectory('config/autoload');
$container->add(ConfigInterface::class, $config, true);

foreach ($config->get('container.invokables') as $instance => $callable) {
    $container->add($instance, $callable, true);
}

return $container;