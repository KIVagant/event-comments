<?php
namespace Kivagant;

/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
return [
    'storage' => [
        'sqlite' => [
            'path' => 'storage/database.sqlite',
        ],
    ],
];