<?php

/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
return [
    'container' => [
        'invokables' => [
            \Kivagant\Application\ApplicationInterface::class => \Kivagant\Application\Application::class,
            // default request
            \Psr\Http\Message\ServerRequestInterface::class => \Kivagant\Http\ServerRequestFactory::class,
            // default response
            \Kivagant\Http\ApplicationResponseInterface::class => \Kivagant\Http\ArrayResponse::class,

            \Kivagant\Router\RouterInterface::class => \Kivagant\Router\Router::class,
            \Kivagant\EventManager\EventManagerInterface::class => \Kivagant\EventManager\EventManager::class,

            \Kivagant\Layers\Sqlite\ConnectionInterface::class => \Kivagant\Layers\Sqlite\Sqlite::class,
            \Kivagant\Layers\Cache\CacheInterface::class => \Kivagant\Layers\Cache\Cache::class,

            \Kivagant\Middleware\ErrorHandlerMiddleware::class => \Kivagant\Middleware\ErrorHandlerMiddleware::class,
            \Kivagant\Comment\Middleware\CommentCreateMiddleware::class => \Kivagant\Comment\Middleware\CommentCreateMiddleware::class,
            \Kivagant\Comment\Middleware\CommentReadMiddleware::class => \Kivagant\Comment\Middleware\CommentReadMiddleware::class,
            \Kivagant\Comment\Middleware\CommentDeleteMiddleware::class => \Kivagant\Comment\Middleware\CommentDeleteMiddleware::class,
            \Kivagant\Middleware\PrepareResponseMiddleware::class => \Kivagant\Middleware\PrepareResponseMiddleware::class,

            \Kivagant\Comment\Service\CommentServiceInterface::class => \Kivagant\Comment\Service\CommentService::class,
            \Kivagant\Comment\Listener\CommentParserListener::class => \Kivagant\Comment\Listener\CommentParserListener::class,

            \Kivagant\Comment\Model\CommentModel::class => \Kivagant\Comment\Model\CommentModel::class,
            \Kivagant\Comment\Model\Table\CommentTable::class => \Kivagant\Comment\Model\Table\CommentTable::class,
        ],
    ],
];