<?php
namespace Kivagant;

return [
    'routes' => [
//        [
//            'name' => 'testroute',
//            'path' => '/{id:[0-9]+/}{firstname:[a-z]+/}somestring/{?:[0-9]+/}',
//            'methods' => ['GET'],
//            'options' => [
//                'middlewares' => [
//                    Middleware\CommentCreateMiddleware::class,
//                ],
//            ],
//        ],
//        [
//            'name' => 'full-comparsion',
//            'path' => '/hello/world/',
//            'methods' => ['GET'],
//            'options' => [
//                'middlewares' => [
//                    Middleware\CommentCreateMiddleware::class,
//                ],
//            ],
//        ],
//        [
//            'name' => 'simple-regexp',
//            'path' => '/{:hello/?}{?:world/?}',
//            'methods' => ['GET'],
//            'options' => [
//                'middlewares' => [
//                    Middleware\CommentCreateMiddleware::class,
//                ],
//            ],
//        ],
        [
            'name' => 'add-comment',
            'path' => '/create/',
            'methods' => ['GET', 'POST'],
            'options' => [
                'middlewares' => [
                    Middleware\ErrorHandlerMiddleware::class,
                    Comment\Middleware\CommentCreateMiddleware::class,
                    Middleware\PrepareResponseMiddleware::class,
                ],
            ],
        ],
        [
            'name' => 'delete-comment',
            'path' => '/delete/{id:[0-9]+/?}',
            'methods' => ['GET', 'POST', 'DELETE'],
            'options' => [
                'middlewares' => [
                    Middleware\ErrorHandlerMiddleware::class,
                    Comment\Middleware\CommentDeleteMiddleware::class,
                    Middleware\PrepareResponseMiddleware::class,
                ],
            ],
        ],
        [
            'name' => 'get-comment',
            'path' => '/{?id:[0-9]+/?}',
            'methods' => ['GET'],
            'options' => [
                'middlewares' => [
                    Middleware\ErrorHandlerMiddleware::class,
                    Comment\Middleware\CommentReadMiddleware::class,
                    Middleware\PrepareResponseMiddleware::class,
                ],
            ],
        ],
    ],
];
