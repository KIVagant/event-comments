<?php
namespace Kivagant;

return [
    'events' => [
        'subscribers' => [
            Comment\Event\CommentAddedEvent::class => [
                Comment\Listener\CommentParserListener::class,
            ],
        ],
    ],
];