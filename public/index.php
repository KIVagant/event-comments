<?php

use Kivagant\Application\ApplicationInterface;

chdir(dirname(__DIR__));

require 'vendor/autoload.php';
$container = require 'config/container.php';

$container->get(ApplicationInterface::class)
    ->run();

exit;