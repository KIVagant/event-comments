# Comments HTTP API service demo project

This is a demo project written from zero without any libraries and frameworks (except interfaces).

## Features

- Service Container and constructor Dependency Injection with factories support;
- PSR-7 Middlewares Pipeline;
- Simple routing with HTTP methods and regexp support;
- Configuration service;
- Event Manager (dispatcher);
- Caching prototype;
- SQLite3 layer;
- Accept headers support: ```application/xhtml+xml``` by default or ```application/json``` by request;
- CRUD API example.

## Installation

PHP^7 required.
Clone this repo and run:

```
composer install
php -S localhost:8000 -t public
```

## Examples

### Read all

```
curl -i -H "Accept: application/json" http://localhost:8000/
HTTP/1.1 200 OK
Host: localhost:8000
Connection: close
X-Powered-By: PHP/7.0.7
1.1
Content-type: application/json

{"data":[{"id":1,"comment":"smile, bro {smile}"},{"id":2,"comment":"{smile} sun shine reggae"}],"request":[],"route":"get-comment","message":{"status":200,"text":"Found"}}
```

### Read by ID
```
curl -i -H "Accept: application/json" http://localhost:8000/2/
HTTP/1.1 200 OK
Host: localhost:8000
Connection: close
X-Powered-By: PHP/7.0.7
1.1
Content-type: application/json

{"data":[{"id":2,"comment":"{smile} sun shine reggae"}],"request":{"id":"2"},"route":"get-comment","message":{"status":200,"text":"Found"}}
```

### Create

```
curl -i -H "Accept: application/json" http://localhost:8000/create/\?comment\=hi%20:\)
HTTP/1.1 200 OK
Host: localhost:8000
Connection: close
X-Powered-By: PHP/7.0.7
1.1
Content-type: application/json

{"data":{"id":3,"comment":"hi {smile}"},"request":[],"route":"add-comment","message":{"status":200,"text":"Found"}}%
```

### Delete

```
curl -i -H "Accept: application/json" http://localhost:8000/delete/3
HTTP/1.1 200 OK
Host: localhost:8000
Connection: close
X-Powered-By: PHP/7.0.7
1.1
Content-type: application/json

{"data":{"deleted":3},"request":{"id":"3"},"route":"delete-comment","message":{"status":200,"text":"Found"}}
```

## Event manager example

See:

- \Kivagant\Comment\Service\CommentService::fireCommentAdded
- \Kivagant\Comment\Listener\CommentParserListener::proceed

## License

Copyright 2016 Eugene Glotov <kivagant at gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
